module PagesHelper

	def site_title
		base_title = "App 01"

		if @page_name.nil?
			base_title
		else
			"#{base_title} | #{@page_name}"
		end 
	end
	
end
