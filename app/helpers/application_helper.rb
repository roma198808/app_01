module ApplicationHelper

	private

	def current_user
		@current_user ||= User.find(session[:user_id]) if session[:user_id]
	end

	def authenticate
    session[:redirect_to] = request.fullpath
    redirect_to signin_path, :notice => "Please, sign in" unless current_user
  end

  def correct_user
  	@user = User.find(params[:id])
  	redirect_to root_path unless @user == current_user
  end

  def admin
    redirect_to root_path unless current_user.admin?
  end
end
