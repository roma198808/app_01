class RelationshipsController < ApplicationController

	def create
		user = User.find(params[:relationship][:followed_id])
		current_user.follow!(user)
		flash.now.alert = "Followed user"
		redirect_to root_path
	end

	def destroy
		current_user.relationships.find(params[:id]).destroy
		flash.now.alert = "Unfollowed user"
		redirect_to root_path
	end

end