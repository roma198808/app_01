class UsersController < ApplicationController
  before_filter :authenticate, :except => [:show, :new, :create]
  before_filter :correct_user, :only => [:edit, :update]
  before_filter :admin, :only => [:index, :destroy]

  def index
      @users = User.paginate(:page => params[:page])
  end

  def new
    @page_name = "Register"
    @user = User.new
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      session[:user_id] = @user.id
      redirect_to @user, :notice => "Logged in!"
    else
      @page_name = "Register"
      render "new"
    end
  end

  def show
    @user = User.find(params[:id])
    @page_name = @user.name
    @microposts = @user.microposts
  end

  def edit
    @page_name = "Edit your profile"
    @user = User.find(params[:id])
  end

  def update
    @page_name = "Edit your profile"    
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user])
      redirect_to @user, :notice => "Profile has been updated"
    else
      flash.now.alert = "Profile wasn't updated"
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash.now.alert = "User has been deleted"
    redirect_to users_path
  end

  def following
    @page_name = "Following"
    @follow = current_user.following.all
    render 'follow'
  end

  def followers
    @page_name = "Followers"
    @follow = current_user.followers.all
    render 'follow'
  end

end
