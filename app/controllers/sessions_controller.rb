class SessionsController < ApplicationController

  def new
    @page_name = "Log In"
  end

  def create
    @user = User.authenticate(params[:email], params[:password])
    if @user
      session[:user_id] = @user.id
      redirect_to (session[:redirect_to] || user_path(@user.id)), :notice => "Logged In!"
    else
      flash.now.alert = "Invalid email or password"
      @page_name = "Log In"
      render "new"
    end
  end

  def destroy
    session[:user_id] = nil
    session[:redirect_to] = nil
    redirect_to root_url, :notice => "Logged out!"
  end

end