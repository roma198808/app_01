class PagesController < ApplicationController

	def home
		@user = current_user
		@micropost = Micropost.new
    	@feed_items = current_user.feed
		if @user
			@page_name = "Homepage for #{@user.name}"
		else
			@page_name = "Home"
		end
	end

	def about
		@page_name = "About"
	end

	def contact
		@page_name = "Contact"
	end

	def help
		@page_name = "Help"
	end
end
