class MicropostsController < ApplicationController
	before_filter :authenticate

	def create
		@micropost = current_user.microposts.build(params[:micropost])
		if @micropost.save
			redirect_to root_path, :notice => "Post was successfully saved"
		else
			@user = current_user
			flash.now.alert = "Error in saving post"
			@feed_items = current_user.feed.all
			render 'pages/home'
		end
	end

	def destroy
		Micropost.find(params[:id]).destroy
		redirect_to root_path
	end
end