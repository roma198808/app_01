require 'faker'

namespace :db do
	desc "Fill database with sample data"
	task :populate => :environment do
		Rake::Task['db:reset'].invoke
		make_users
		make_microposts
		make_relationships
	end
end

def make_users
	admin = User.create!(name: 'Raman Sycheuski',
						 email: 'test@one.com',
						 password: 'ramanadmin',
						 password_confirmation: 'ramanadmin')
	admin.toggle!(:admin)
	99.times do |n|
		name = Faker::Name.name
		email = "fake#{n+2}@email.com"
		password = 'password'
		password_confirmation = 'password'
		User.create!(name: name,
					 email: email,
					 password: password,
					 password_confirmation: password_confirmation)			
	end
end

def make_microposts
	User.all(:limit => 50).each do |user|
		5.times do |n|
			user.microposts.create!(content: "#{user.name} -- Post number #{n}")
		end
	end
end

def make_relationships
	users = User.all
	user = users.first
	following = users[2..5]
	followers = users[3..10]
	following.each { |followed| user.follow!(followed) }
	followers.each { |follower| follower.follow!(user) }
end